// C++ code for power function
#include <bits/stdc++.h>
using namespace std;

/* Works only if a >= 0 and b >= 0 */
long long int pow(long long int a, long long int b)
	{ cout<<" pws "<<endl;
		
		if (b == 0) 	return 1;
		long long int answer = a;	int increment = a;
	long long int i, j;
	for(i = 1; i < b; i++)
	{
		for(j = 1; j < a; j++)
		{
			answer += increment;
		}
		increment = answer;
		cout<<" answer "<<answer<<endl;
	}
	
	return answer;
}

// Driver Code
int main()
{	long long int  power;
	cout <<" * * * * * "<<endl;
	for (int n = 1; n < 21;n++){ 
		cout <<" n "<<n<<" "<< pow(2,n);
		cout <<endl;
	}
	return 0;
}

// This code is contributed
// by rathbhupendra
// https://www.geeksforgeeks.org/write-you-own-power-without-using-multiplication-and-division/
