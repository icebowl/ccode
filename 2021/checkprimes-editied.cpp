// file: checkprimes.cpp
#include <iostream>
#include <cmath>
using namespace std;

bool isprime(long long int p){
	bool prime = true;
	long long int mff,divisor; // mff(middle first factor)
	mff = (long long int)sqrt(p)+1;
	 //cout<<p<<" "<<mff<<endl;
	//long long int middlefactor =  (long long int)longsqrt ;
	divisor = 3;
	while (divisor < mff && prime == true){
		if(p % divisor==0){
			prime = false;
		}
		divisor++;
	}
	//cout<<p<< " - "<<prime<<endl;
	return prime;
}

// main function
int main(){
long long int n;
int count = 1;
n = 3;
bool prime;
cout<<"NUMBER | PRIME COUNT | prime (0=false 1=true)\n";
cout<<"2 \t\t 1"<<endl;
	while (n < 65536){
		//cout <<n<<endl;
		prime = isprime(n);
		if (prime) {
			count++;
			// add this line
			cout<<"NUMBER | PRIME COUNT | prime (0=false 1=true)\n";
			cout<<n<<"\t\t"<<count<<"\t\t"<<prime<<endl;
		}
		n = n + 2;
		
	}
}

// long long int
// 9223372036854775807
