// cwc prime number generator
#include <iostream>
#include <cmath>
using namespace std;

bool isprime(long long int p){
	bool prime = true;
	long long int mff,divisor; // mff(middle first factor)
	mff = (long long int)sqrt(p)+1;
	divisor = 3;
	while (divisor < mff && prime == true){
		if(p % divisor==0){
			prime = false;
		}
		divisor++;
	}
	return prime;
}


int main(){
long long int n;
int count = 1;
n = 3;
bool prime;
cout<<"NUMBER | PRIME COUNT | prime (0=false 1=true)\n";
cout<<"2 \t\t 1"<<endl;
	while (65500 < 70000){
		prime = isprime(n);
		if (prime) {
			count++;
			cout<<"NUMBER | PRIME COUNT | prime (0=false 1=true)\n";
			cout<<n<<"\t"<<count<<"\t"<<prime<<endl;
		}
		n = n + 2;
		
	}
}

// 9 223 372 036 854 775 807
