//xor
// https://kylewbanks.com/blog/Simple-XOR-Encryption-Decryption-in-Cpp
#include <bits/stdc++.h> 
#include <iostream>
#include <array>

using namespace std;

string encryptDecrypt(string toEncrypt,char key) {
    //char key = 'K'; //Any char will work
    string output = toEncrypt;
    
    for (int i = 0; i < toEncrypt.size(); i++)
        output[i] = toEncrypt[i] ^ key;
    
    return output;
}


int main(int argc, char** argv)
{   int n;
	string inputstr = argv[1];
	string keystr = argv[2];
	// string to char
	char keylist[keystr.length() + 1]; 
    strcpy(keylist, keystr.c_str()); 
	char key = keylist[0];
	//char key = 'k';
	cout<<inputstr<<" "<<key<<endl;
	//encrypt
	string encrypted = encryptDecrypt(inputstr,key);
	cout<<"encrypted "<<encrypted<<" * "<<endl;
	// encryted to char
	// string to char
	char enclist[encrypted.length() + 1]; 
    strcpy(enclist, encrypted.c_str()); 
	
	
	for (n = 0; n < inputstr.length();n++){
		cout<<int(inputstr[n])<<" ";
	}
	cout<<"\n\n";
	for (n = 0; n < sizeof(enclist)-1;n++){
		cout<<enclist[n]<<" ";
	}
	cout<<"\n\n";
	for (n = 0; n < sizeof(enclist)-1;n++){
		cout<<int(enclist[n])<<" ";
	}
	
	
    return 0;
}
